import { Component, Input, OnInit } from '@angular/core';
import { City } from '../city.model';
import { CitiesService } from '../cities.service';

@Component({
  selector: 'app-city',
  templateUrl: './city.component.html',
  styleUrls: ['./city.component.css']
})
export class CityComponent implements OnInit {

  @Input() city: City

  roundTemp = null;

  constructor(private citiesService: CitiesService) {}

  ngOnInit() {
    this.roundTemp = Math.round(this.city.temp);
  }

  onDelete() {
    this.citiesService.deleteCity(this.city);
  }

  async onUpdateWeather() {
    const weatherData = await this.citiesService.getWeather(this.city.lat, this.city.lon);
    this.city.temp = weatherData.list["0"].main.temp;
    this.citiesService.updateCity(this.city);
  }

}
