import { Component, OnInit } from '@angular/core';
import { City } from '../city.model';
import { CitiesService } from '../cities.service';

@Component({
  selector: 'app-cities-form',
  templateUrl: './cities-form.component.html',
  styleUrls: ['./cities-form.component.css']
})
export class CitiesFormComponent implements OnInit{

  enterName: string = "";

  constructor(private citiesService: CitiesService) {}

  ngOnInit() {
    this.citiesService.loadCities();
  }

  async onAdd() {
    const coordsData = await this.citiesService.getCoord(this.enterName);
    const coord = coordsData.response.GeoObjectCollection.featureMember["0"].GeoObject.Point.pos;
    const name = coordsData.response.GeoObjectCollection.featureMember["0"].GeoObject.name;
    const region = coordsData.response.GeoObjectCollection.featureMember["0"].GeoObject.description;
    const lat = +coord.split(' ')[1];
    const lon = +coord.split(' ')[0];
    const weatherData = await this.citiesService.getWeather(lat, lon);
    const temp = weatherData.list["0"].main.temp;

    const city = new City(
      name,
      region,
      lat,
      lon,
      temp
    );

    this.citiesService.addCity(city)

    this.enterName = '';
  }

}
