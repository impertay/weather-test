export class City {
    constructor(
        public name: string,
        public region: string,
        public lat: number,
        public lon: number,
        public temp: number,
        public id?: number
    ) {}
}

export interface Cities {
    cities: City[]
}
