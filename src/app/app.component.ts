import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Cities } from './city.model';
import { AppState } from './redux/app.state';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  public citiesState: Observable<Cities>

  constructor(private store: Store<AppState>) {}

  ngOnInit() {
    this.citiesState = this.store.select('cityPage')
  }
}
