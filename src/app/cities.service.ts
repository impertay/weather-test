import { Injectable } from "@angular/core";
import { Http, Response } from "@angular/http";
import { Store } from "@ngrx/store";
import { AppState } from "./redux/app.state";
import { LoadCities, AddCity, DeleteCity, UpdateCity } from "./redux/cities.action";
import { City } from "./city.model";

@Injectable()
export class CitiesService {

    static DB_URL: string = 'http://localhost:3000/';
    static GEO_URL: string = 'https://geocode-maps.yandex.ru/1.x/?format=json&geocode=';
    static WEATHER_URL: string = 'http://api.openweathermap.org/data/2.5/forecast?';
    static APPID: string = 'a798f15605f64aac80e230e7e1663a90';

    constructor(private http: Http, private store: Store<AppState>) {}

    async getCoord(enterName) {
        return await this.http.get(CitiesService.GEO_URL + enterName)
        .map((response: Response) => response.json())
        .toPromise();
    }

    async getWeather(lat, lon) {
        return await this.http.get(CitiesService.WEATHER_URL + 'lat=' + lat + '&lon=' + lon + '&units=metric&APPID=' + CitiesService.APPID)
        .map((response: Response) => response.json())
        .toPromise();
    }

    preloadCities() {
        return this.http.get(CitiesService.DB_URL + 'cities')
        .map((response: Response) => response.json());
    }

    loadCities() {
        this.preloadCities()
        .subscribe((citiesData) => {
            this.store.dispatch(new LoadCities(citiesData))
        })
    }

    addCity(city: City) {
        this.http.post(CitiesService.DB_URL + 'cities', city)
        .map((response: Response) => response.json())
        .subscribe((city: City) => {
            this.store.dispatch(new AddCity(city))
        })
    }

    deleteCity(city: City) {
        this.http.delete(CitiesService.DB_URL + 'cities/' + city.id)
        .map((response: Response) => response.json())
        .subscribe(() => {
            this.store.dispatch(new DeleteCity(city))
        })
    }

    updateCity(city: City) {
        this.http.put(CitiesService.DB_URL + 'cities/' + city.id, city)
        .map((response: Response) => response.json())
        .subscribe((city: City) => {
            this.store.dispatch(new UpdateCity(city))
        })
    }

}
