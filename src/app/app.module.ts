import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { AppComponent } from './app.component';
import { CitiesFormComponent } from './cities-form/cities-form.component';
import { CityComponent } from './city/city.component';
import { CitiesService } from './cities.service';
import { citiesReducer } from './redux/cities.reducer';
import { CitiesEffect } from './redux/cities.effect';

@NgModule({
  declarations: [
    AppComponent,
    CitiesFormComponent,
    CityComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    StoreModule.forRoot({cityPage: citiesReducer}),
    HttpModule,
    EffectsModule.forRoot([CitiesEffect])
  ],
  providers: [CitiesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
