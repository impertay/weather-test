import { City } from "../city.model";

export interface AppState {
    cityPage: {
        cities: City[]
    }
}
