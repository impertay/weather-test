import { Injectable } from '@angular/core'
import { Actions, Effect } from '@ngrx/effects'
import { CITY_ACTION, AddCity } from './cities.action';
import { City } from '../city.model';
import { CitiesService } from '../cities.service';

@Injectable()
export class CitiesEffect {

    constructor(private actions$: Actions, private citiesService: CitiesService) {}

    @Effect() loadCities = this.actions$.ofType(CITY_ACTION.ADD_CITY)
        .switchMap(() => {
            return this.citiesService.preloadCities()
        })
        .mergeMap((cities : City[]) => {
            return [
                {
                    type: CITY_ACTION.LOAD_CITIES,
                    payload: cities
                }
            ]
        })

}
