import { CITY_ACTION, CitiesAction } from "./cities.action";

const initialState = {
    cities: []
}

export function citiesReducer(state = initialState, action: CitiesAction) {
    switch(action.type) {
        case CITY_ACTION.ADD_CITY:
            return {
                ...state,
                cities: [...state.cities, action.payload]
            }
        case CITY_ACTION.DELETE_CITY:
            return {
                ...state,
                cities: [...state.cities.filter(c => c.id !== action.payload.id)]
            }
        case CITY_ACTION.UPDATE_CITY:
            return {
                ...state,
                cities: [...state.cities]
            }
        case CITY_ACTION.LOAD_CITIES:
            return {
                ...state,
                cities: [...action.payload]
            }
        default:
            return state
    }
}
