import { Action } from "@ngrx/store";
import { City } from "../city.model";

export namespace CITY_ACTION {
    export const ADD_CITY = 'ADD_CITY'
    export const DELETE_CITY = 'DELETE_CITY'
    export const UPDATE_CITY = 'UPDATE_CITY'
    export const LOAD_CITIES = 'LOAD_CITY'
}

export class AddCity implements Action {
    readonly type = CITY_ACTION.ADD_CITY

    constructor(public payload: City) {}
}

export class DeleteCity implements Action {
    readonly type = CITY_ACTION.DELETE_CITY

    constructor(public payload: City) {}
}

export class UpdateCity implements Action {
    readonly type = CITY_ACTION.UPDATE_CITY

    constructor(public payload: City) {}
}

export class LoadCities implements Action {
    readonly type = CITY_ACTION.LOAD_CITIES

    constructor(public payload: City[]) {}
}

export type CitiesAction = AddCity | DeleteCity | UpdateCity | LoadCities
